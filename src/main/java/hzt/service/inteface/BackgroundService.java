package hzt.service.inteface;

import hzt.model.appearance.Resource;

import java.util.Set;

public interface BackgroundService {

    Set<Resource> getResources();
}
